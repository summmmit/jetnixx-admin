// server.js
var package_json = require('./package'),
    ENV_File = require("./ENV.json");

var host = ENV_File.ENVIRONMENT.APP_.HOST;
var port = process.env.PORT || 8080;
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// configuration ===============================================================
var express = require('./server/config/express');
var app = express();
// launch ======================================================================
var server = app.listen(port, function () {

    console.log('Current Environment: %s', app.settings.env);
    console.log('%s App Version: %s listening host %s at port:%s', package_json.name, package_json.version, host, port);
});

module.exports = app;
