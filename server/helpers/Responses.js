// loading user constants
var CONSTANTS = require('./../helpers/constants');

module.exports = {
    result: function (statusCode, statusText, data) {

        var result = {
            'data': data != null ? data : null,
            'statusText': statusText
        }
        return result;
    }
}
