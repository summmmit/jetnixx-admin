// set up ======================================================================
var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var validator = require('express-validator');
var config = require('./config');
var favicon = require('serve-favicon');
var oauthserver = require('node-oauth2-server');
var route = require('./../routes');

module.exports = function () {

    //-------------------------------------DEBUG-------------------------------------------
    console.log("Starting Application---------------------------------------------------------------");

    var app = express();
    require('./passport')(passport); // pass passport for configuration

    //Mongoose configuration ===============================================================
    mongoose.set('debug', true);
    mongoose.connect(config.DATABASE); // connect to our database
    mongoose.connection.on('error', function () {
        console.log('Mongoose connection error');
    });
    mongoose.connection.once('open', function callback() {
        console.log("Mongoose connected to the database");
    });

    if (process.env.NODE_ENV === 'development') {
        app.use(morgan('dev'));
    }

    app.use(cookieParser()); // read cookies (needed for auth)

    // Body Parser        ----------------------------------------------------------------To get Information from the Form------------------------------------------------
    app.use(bodyParser()); // get information from html forms
    app.use(bodyParser.json()); // get information in json format
    app.use(bodyParser.urlencoded({
        extended: true
    })); // get information in urlEncodedForm

    // HTTP Request handling configurations--------------------------------------------------------------------------------
    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
        next();
    });

    // Set Session variables
    app.set('SESSION_SECRET', config.ENV.SESSION_.SECRET); // secret variable

    // Express Validator-------------------------------for parameters validations--------------------
    app.use(validator());

    // EJS -------------------------------------------------------------------------------Setting Templating Engine------------------------------------------------
    app.set('view engine', 'ejs'); // set up ejs for templating

    // Passport            -----------------------------------------------------------------------Passport Settings-----------------------------------------
    app.use(session({
            saveUninitialized: true,
            resave: true,
            secret: config.ENV.SESSION_.SECRET,
            cookie: {
                maxAge: config.ENV.SESSION_.COOKIE_MAX_AGE,
                expires: config.ENV.SESSION_.COOKIE_MAX_AGE
            }
        })
    );
    app.use(passport.initialize());
    app.use(passport.session()); // persistent login sessions
    app.use(flash()); // use connect-flash for flash messages stored in session

    app.use(express.static(__dirname + './../../client'));
    app.use('/bower_components', express.static(__dirname + './../../bower_components'));

    app.use(favicon(__dirname + './../../client/favicon.ico'));

    //----*************************************OAUTH2 Server *********************************---

    var models = require('./../models');

    app.oauth = oauthserver({
        model: models.oauth,
        grants: ['password', 'authorization_code', 'refresh_token'],
        debug: true
    });

    app.all('/oauth/token', app.oauth.grant());

    // routes for oauth server
    app.use(route(passport));

    app.use(function (err, req, res, next) {
        res.status(err.statusCode || 500).json(err);
    });

    return app;
}