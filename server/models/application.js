// load the things we need
var mongoose = require('mongoose');
var CONSTANTS = require('./../helpers/constants');

// define the schema for our user model
var ApplicationSchema = mongoose.Schema({
        is_installed: {
            type: Number,
            required: true,
            enum: [
                CONSTANTS.APPLICATION_.INSTALLED_.NO,
                CONSTANTS.APPLICATION_.INSTALLED_.YES
            ]
        },
        current_version: {
            type: String,
            required: true,
            index: true
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);
// create the model and expose it to our app
module.exports = mongoose.model('applications', ApplicationSchema);
