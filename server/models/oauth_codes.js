// load the things we need
var mongoose = require('mongoose');
var CONSTANTS = require('./../helpers/constants');
var bcrypt = require('bcrypt-nodejs');
var randomString = require("randomstring");

// define the schema for our user model
var OAuthCodesSchema = mongoose.Schema({
        _user_access_id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'user_access'
        },
        client_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
            ref: 'oauth_clients'
        },
        code: {
            type: String,
            required: true,
            index: true
        },
        scopes: {
            type: String,
        },
        is_revoked: {
            type: Number,
            required: true,
            enum: [
                CONSTANTS.REVOKED.REVOKED,
                CONSTANTS.REVOKED.NOT_REVOKED
            ]
        },
        expires_at: {
            type: Date
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);

// generating a hash
OAuthCodesSchema.methods.generateCode = function () {
    return bcrypt.hashSync(randomString.generate(10), bcrypt.genSaltSync(8), null);
};
// checking if password is valid
OAuthCodesSchema.methods.validateCode = function (code) {
    return bcrypt.compareSync(code, this.code);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('oauth_codes', OAuthCodesSchema);
