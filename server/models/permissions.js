// load the things we need
var mongoose = require('mongoose');
var CONSTANT = require('./../helpers/constants');
// define the schema for our user model
var PermissionsSchema = mongoose.Schema({

        _user_access_id: {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
        },
        name: {
            type: String,
            required: true,
            index: true
        },
        display_name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        is_removable: {
            type: Number,
            required: true,
            enum: [
                CONSTANT.PERMISSION_REMOVABLE.NON_REMOVABLE,
                CONSTANT.PERMISSION_REMOVABLE.REMOVABLE
            ]
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);
// create the model for users and expose it to our app
module.exports = mongoose.model('permissions', PermissionsSchema);
