// load the things we need
var mongoose = require('mongoose');
var CONSTANT = require('./../helpers/constants');

// define the schema for our user model
var userAccessDetailsSchema = mongoose.Schema({

        _user_access_id: {
            type: mongoose.Schema.Types.ObjectId,
            required: true,
        },
        access_token: {
            type: String,
            required: true,
            index: true
        },
        ip_address: {
            type: String,
            required: true,
            index: true
        },
        login_at: {
            type: Date,
            default: Date.now,
        },
        user_agent: {
            type: String,
            required: true,
            index: true
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);

// add and update the date on Every save

// userAccessDetailsSchema.pre('save', function (next) {
//
//     var currentDate = new Date();
//
//     this.updated_at = currentDate;
//
//     if (!this.created_at) {
//         this.created_at = currentDate;
//     }
//
//     next();
// });

// create the model for users and expose it to our app
module.exports = mongoose.model('user_access_details', userAccessDetailsSchema);