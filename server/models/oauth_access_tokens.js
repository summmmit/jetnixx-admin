var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var OAuthAccessTokensSchema = new Schema({
    access_token: {
        type: String,
        required: true,
        unique: true
    },
    clientId: String,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user_access'
    },
    expires: {
        type: Date
    }
});

mongoose.model('oauth_access_tokens', OAuthAccessTokensSchema);
var OAuthAccessTokensModel = mongoose.model('oauth_access_tokens');

module.exports.getAccessToken = function (bearerToken, callback) {
    OAuthAccessTokensModel.findOne({access_token: bearerToken}, callback);
};

module.exports.saveAccessToken = function (token, clientId, expires, userId, callback) {

    var fields = {
        clientId: clientId,
        userId: userId,
        expires: expires
    };

    OAuthAccessTokensModel.update({access_token: token}, fields, {upsert: true}, function (err) {
        if (err) {
            console.error(err);
        }
        callback(err);
    });
};
