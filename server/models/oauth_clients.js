// load the things we need
var mongoose = require('mongoose');
var CONSTANTS = require('./../helpers/constants');
var Activities = require('./activities');

// define the schema for our user model
var OAuthClientsSchema = mongoose.Schema({
        _user_access_id: {
            type: String
        },
        client_name: {
            type: String,
            unique: true,
            required: true,
        },
        client_secret: {
            type: String,
            required: true,
        },
        redirect_url: {
            type: String,
            required: true,
        },
        client_type: {
            type: Number,
            required: true,
            enum: [
                CONSTANTS.CLIENT_TYPE.PERSONAL_GRANT_TYPE,
                CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE,
                CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE,
            ]
        },
        is_revoked: {
            type: Number,
            required: true,
            default: CONSTANTS.REVOKED.NOT_REVOKED,
            enum: [
                CONSTANTS.REVOKED.REVOKED,
                CONSTANTS.REVOKED.NOT_REVOKED
            ]
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);

OAuthClientsSchema.static('getClient', function (clientId, clientSecret, callback) {
    var params = {
        _id: clientId
    };
    if (clientSecret != null) {
        params.client_secret = clientSecret;
    }
    OAuthClientsModel.findOne(params, callback);
});

OAuthClientsSchema.static('grantTypeAllowed', function (clientId, grantType, callback) {

    if (grantType === 'password') {

        OAuthClientsModel.findOne({
                $and: [{
                    _id: clientId
                }, {
                    client_type: CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE
                }]
            },
            function (err, client) {
                if (err || !client) {
                    return callback(false, false);
                }

                return callback(false, true);
            });
    }
    else if (grantType === 'authorization_code') {

        OAuthClientsModel.findOne({
                $and: [{
                    _id: clientId
                }, {
                    client_type: CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE
                }]
            },
            function (err, client) {
                if (err || !client) {
                    return callback(false, false);
                }

                return callback(false, true);
            });
    }
    else {

        callback(false, true);
    }
});
// add and update the date on Every save
OAuthClientsSchema.pre('save', function (next) {

    var currentDate = new Date();
    this.updated_at = currentDate;

    if (this.client_name && this.isModified('client_name')) {

        var newActivity = new Activities();
        newActivity._user_access_id = this._user_access_id;
        newActivity.activity_type = CONSTANTS.ACTIVITY_TYPES.NEW_CLIENT_CREATION_ACTIVITY;
        newActivity.activity_text = 'New Client Created';
        newActivity.icon = 'mdi-action-face-unlock';

        newActivity.save(function (err) {
            if (err) {
                console.log('Error in saving activity: OAuthClients->Client Creation/Updation Activity');
                throw err;
            }
        });
    }
    next();
})

// create the model for clients and expose it to our app
mongoose.model('oauth_clients', OAuthClientsSchema);
var OAuthClientsModel = mongoose.model('oauth_clients');
module.exports = OAuthClientsModel;
