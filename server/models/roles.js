// load the things we need
var mongoose = require('mongoose');
var CONSTANT = require('./../helpers/constants');

// define the schema for our user model
var RolesSchema = mongoose.Schema({

        _user_access_id: {
            type: mongoose.Schema.Types.ObjectId,
            index: true,
            required: true,
        },
        name: {
            type: String,
            required: true,
            index: true
        },
        display_name: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        is_removable: {
            type: Number,
            required: true,
            default: CONSTANT.ROLE_REMOVABLE.NON_REMOVABLE,
            enum: [
                CONSTANT.ROLE_REMOVABLE.NON_REMOVABLE,
                CONSTANT.ROLE_REMOVABLE.REMOVABLE
            ]
        }
    },
    {
        timestamps: {
            createdAt: 'created_at',
            updatedAt: 'updated_at'
        }
    }
);
// create the model for users and expose it to our app
module.exports = mongoose.model('roles', RolesSchema);
