var util = require('util');

function NotFound(message) {
    this.code = 404;
    this.message = message;
    Error.call(this);
}

util.inherits(NotFound, Error);