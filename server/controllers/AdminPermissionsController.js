var userDetails = require('./../models').user_details,
    UserAccess = require('./../models').user_access,
    Permissions = require('./../models').permissions,
    Roles_to_Permissions = require('./../models').permissions_roles,
    Roles = require('./../models').roles,
    CONSTANTS = require('./../helpers/constants'),
    ResultResponses = require('./../helpers/resultResponses'),
    async = require('async');

var self = {

    getPermission: function (req, res) {

        var result = {};
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        Permissions.findOne({_id: req.params.permission_id}, function (err, permission) {
            if (err) {
                console.log('Error While fetching Permissions: PermissionsController->getPermission');
                throw err;
            }

            if (!permission) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'Permission not available!!', permission);
            } else {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', permission);
            }

            res.json({'result': result})
        });
    },
    getAllPermissions: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        Permissions.find({},
            {
                __v: 0
            },
            {
                skip: parseInt(offset),
                limit: parseInt(limit),
                sort: {
                    created_at: -1 //Sort by activity_time Added ASC
                }
            },
            function (err, permissions) {
                if (err) {
                    console.log('Error While fetching Permissions: PermissionsController->getAllPermissions');
                    throw err;
                }
                data = {
                    limit: parseInt(limit),
                    offset: parseInt(offset)
                }
                Permissions.count({}, function (err, TotalCount) {
                    if (err) {
                        console.log('Error While fetching Permissions count: PermissionsController->getAllPermissions');
                        throw err;
                    }

                    data.count = TotalCount;
                    data.permissions = permissions;

                    result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                        'Successfully Fetched!!', data);

                    res.json({'result': result})
                })
            });
    },
    postPermissions: function (req, res) {

        var result = {};

        var name = req.body.name;
        var display_name = req.body.display_name;
        var description = req.body.description;
        var is_removable = req.body.is_removable ? req.body.is_removable : CONSTANTS.ROLE_REMOVABLE.NON_REMOVABLE;
        var _user_access_id = req.member._id;

        var admin_permissions = new Permissions();
        admin_permissions.name = name;
        admin_permissions.display_name = display_name;
        admin_permissions.description = description;
        admin_permissions.is_removable = is_removable;
        admin_permissions._user_access_id = _user_access_id;

        admin_permissions.save(function (err) {
            if (err) {
                console.log('Permissions Save Error: PermissionsController->postPermissions');
                throw err;
            }
            result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                'Successfully Created!!', admin_permissions);

            res.json({'result': result})
        });
    },
    deletePermissions: function (req, res) {

        var result = {};

        var permission_id = req.params.permission_id;

        Permissions.findOne({_id: permission_id}, function (err, permission) {

            if (err) {
                console.log('Permissions Fetching Error: PermissionsController->deletePermissions');
                throw err;
            }

            if (!permission) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'role does not exists!!', null);
            } else {

                permission.remove(function (err) {
                    if (err) {
                        console.log('Permissions Deletion Error: PermissionsController->deletePermissions');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Deleted!!', permission);
            }
            res.json({'result': result})
        })
    },
    putPermissions: function (req, res) {

        var result = {};

        var name = req.body.name;
        var display_name = req.body.display_name;
        var description = req.body.description;
        var is_removable = req.body.is_removable ? req.body.is_removable : CONSTANTS.ROLE_REMOVABLE.NON_REMOVABLE;

        Permissions.findOne({_id: req.params.permission_id}, function (err, permission) {

            if (err) {
                console.log('Permissions Fetching Error: PermissionsController->PutPermissions');
                throw err;
            }

            if (!permission) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'Permission does not exists!!', null);
            } else {

                permission.name = name;
                permission.display_name = display_name;
                permission.description = description;
                permission.is_removable = is_removable;

                permission.save(function (err) {
                    if (err) {
                        console.log('Permission Modification Error: PermissionsController->PutPermissions');
                        throw err;
                    }
                });
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Modified!!', permission);
            }
            res.json({'result': result})
        })
    },
    postAssignPermissionToRole: function (req, res) {

        var role_id = req.body.role_id,
            permission_id = req.body.permission_id,
            result = {};

        async.parallel({
            role: function (callback) {
                Roles.findOne({_id: role_id}, function (err, role) {
                    if (err) {
                        console.log('Permissions Fetching Role Error: PermissionsController->assignPermissionToRole');
                        callback(err);
                    }
                    if (!role) {
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                            'Role does not exists!!', req.body);
                        res.json({'result': result})
                    } else {
                        callback(null, role);
                    }
                })
            },
            permission: function (callback) {
                Permissions.findOne({_id: permission_id}, function (err, permission) {
                    if (err) {
                        console.log('Permissions Fetching Permission Error: PermissionsController->assignPermissionToRole');
                        callback(err);
                    }
                    if (!permission) {
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                            'Permission does not exists!!', req.body);
                        res.json({'result': result})
                    } else {
                        callback(null, permission);
                    }
                })
            },
            role_to_permission: function (callback) {

                var role_to_permission = new Roles_to_Permissions();
                role_to_permission.permission_id = req.body.permission_id;
                role_to_permission.role_id = req.body.role_id;

                role_to_permission.save(function (err, saved_object) {
                    if (err) {
                        console.log('Permissions saving Error: PermissionsController->assignPermissionToRole');
                        callback(err);
                    }

                    callback(null, saved_object);
                })
            }
        }, function (err, result) {
            if (err) {
                console.log('Permissions Error: PermissionsController->assignPermissionToRole');
                callback(err);
            }
            var final_result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                'Successfully Created!!', result.role_to_permission);
            res.json({'result': final_result});
        })
    },
    getAllRolesToPermission: function (req, res) {

        var result = {};
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var data = {};

        async.waterfall([
            function (callback) {

                Permissions.findOne({_id: req.params.permission_id}, function (err, permission) {
                    if (err) {
                        console.log('Error While fetching Permissions: PermissionsController->getAllRolesToPermission');
                        callback(err)
                    }

                    if (!permission) {

                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                            'Permission not available!!', permission);
                        res.json({'result': result})
                    } else {
                        callback(null, permission)
                    }
                })
            },
            function (data, callback) {


                Roles.find({}, function (err, roles) {
                    if (err) {
                        console.log('Error While fetching Roles: PermissionsController->getAllRolesToPermission');
                        throw err;
                    }

                    data = {
                        permission: data,
                        roles: roles
                    };

                    callback(null, data)
                })
            },
            function (data, callback) {

                var role_array = [];

                for (var i = 0; i < data.roles.length; i++) {
                    console.log("role_to_permission----------start");
                    var role_to_permission = self.getRolesToPermissions(req, res, data.roles[i]._id, req.params.permission_id);
                    console.log(role_to_permission);
                    console.log("role_to_permission----------end");
                }
                callback(null, data)
            }
        ], function (err, result) {
            if (err) {
                console.log('Error While Doing async request: PermissionsController->getAllRolesToPermission');
                throw err;
            }
            res.json({'result': result})
        })
    },
    getRolesToPermissions: function (req, res, role_id, permission_id) {

        console.log("---role_to_permission----");
        Roles_to_Permissions.find({$and: [{role_id: role_id}, {permission_id: permission_id}]},
            function (err, role_to_permission) {

                if (err) {
                    console.log('Error While Doing async request: PermissionsController->getAllRolesToPermission');
                    throw err;
                }
                res.json(role_to_permission);
            })
    }
}

module.exports = self;
