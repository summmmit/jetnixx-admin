var userDetails = require('./../models/user_details');
var Notifications = require('./../models/notifications');
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var OAuthClients = require('./../models/oauth_clients');
var bcrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");


module.exports = {

    postApplication: function (req, res, next) {

        var result = {};
        var client_type = parseInt(req.body.client_type);
        var client_name = req.body.client_name;
        var user_id = req.member._id;  // using access token
        var redirect = req.body.redirect_url;
        var revoked = req.body.is_revoked;

        OAuthClients.find({
                $or: [{
                    client_name: client_name
                }, {
                    redirect_url: redirect
                }]
            },
            function (err, oauthClients) {

                if (err) {
                    console.log('Error: OAuthUserApplicationsController->postApplication');
                    throw err;
                }

                if (oauthClients.length > 0) {

                    result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.CONFLICT,
                        'Client Already exists!!', null);
                    res.json({
                        'result': result
                    })
                }
                else {

                    var oauthClients = new OAuthClients();
                    oauthClients.client_name = client_name;
                    oauthClients.client_secret = bcrypt.hashSync(randomstring.generate(10), bcrypt.genSaltSync(8), null);
                    oauthClients.redirect_url = redirect;
                    oauthClients.is_revoked = revoked;

                    if (client_type == CONSTANTS.CLIENT_TYPE.PERSONAL_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.PERSONAL_GRANT_TYPE;
                    }
                    else if (client_type == CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE;
                    }
                    else if (client_type == CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE;
                    }
                    oauthClients._user_access_id = user_id;
                    oauthClients.save(function (err) {
                        if (err) {
                            console.log('OAuthClients Save Error: OAuthUserApplicationsController->postApplication');
                            throw err;
                        }
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                            'Successfully Created!!', oauthClients);
                        res.json({
                            'result': result
                        })
                    });
                }
            })
    },
    authenticateApplication: function (req, res, next) {

        var result = {};

        var client_id = req.params.client_id;

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        OAuthClients.findOne({
            _id: client_id
        }, {
            __v: 0
        }, function (err, oauthClients) {

            if (err) {
                console.log('Error While fetching OAuthClients: OAuthController->authenticateOAuthClients');
                throw err;
            }

            if (oauthClients instanceof OAuthClients) {
                result = true;
            }
            else {
                result = false;
            }

            res.json({
                'result': result
            })
        });
    },
    getAllApplications: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {};

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        OAuthClients.find({}, {
            __v: 0
        }, {
            skip: parseInt(offset),
            limit: parseInt(limit),
            sort: {
                activity_time: 1 //Sort by activity_time Added ASC
            }
        }, function (err, oauthClients) {

            if (err) {
                console.log('Error While fetching OAuthClients: OAuthUserApplicationsController->getAllApplications');
                throw err;
            }

            data = {
                limit: parseInt(limit),
                offset: parseInt(offset)
            }

            OAuthClients.count({}, function (err, TotalCount) {

                data.count = TotalCount;
                data.oauthClients = oauthClients;

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', data);

                res.json({
                    'result': result
                })
            })
        });
    },
    patchApplication: function (req, res) {

        console.log(req.param)

        var result = {};

        var client_id = req.body._id;
        var redirect = req.body.redirect_url;
        var revoked = req.body.is_revoked;

        OAuthClients.findOne({
            _id: client_id
        }, function (err, oauthClients) {

            if (err) {
                console.log('Error: OAuthController->patchOAuthClients');
                throw err;
            }

            if (!oauthClients) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Client does not exists!!', oauthClients);
            }
            else {

                oauthClients.redirect_url = redirect;
                oauthClients.is_revoked = revoked;

                oauthClients.save(function (err) {
                    if (err) {
                        console.log('OAuthClients Save Error: OAuthController->patchOAuthClients');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Updated!!', oauthClients);
            }
            res.json({
                'result': result
            })
        })
    },
    deleteApplication: function (req, res) {

        var result = {};

        var client_id = req.params.application_id;

        OAuthClients.findOne({
            _id: client_id
        }, function (err, oauthClients) {

            if (err) {
                console.log('Error: OAuthController->deleteOAuthClients');
                throw err;
            }

            if (!oauthClients) {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Client does not exists!!', oauthClients);
            }
            else {

                oauthClients.remove(function (err) {
                    if (err) {
                        console.log('OAuthClients Deletion Error: OAuthController->deleteOAuthClients');
                        throw err;
                    }
                });

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Deleted!!', oauthClients);
            }
            res.json({
                'result': result
            })
        })
    }
}
