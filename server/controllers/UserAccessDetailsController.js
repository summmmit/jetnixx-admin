var UserDetails = require('./../models').user_details,
    UserAccessDetails = require('./../models').user_access_details,
    UserAccess = require('./../models').user_access;
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var CheckUserType = require('./../helpers/checkUserType');
var AfterLogin = require('./../middlewares/after_login');
var OAuthAccessTokenController = require('./../controllers').OAuthAccessTokenController;
var request = require('request');
var ENV_File = require("./../../ENV.json");
var mongoose = require('mongoose');
var moment = require('moment');

module.exports = {

    postUserAccessDetails: function (req, res, next) {

        var result;
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var user_access_details = new UserAccessDetails();
        user_access_details.ip_address = req.body.ip_address;
        user_access_details.access_token = req.body._access_token;
        user_access_details.login_at = req.body.login_at;
        user_access_details.user_agent = req.body.user_agent;
        user_access_details._user_access_id = req.member._id;

        user_access_details.save(function (err) {
            if (err) {
                console.log('user_access_details Save Error: userAccessDetailsController->postUserAccessDetails');
                throw err;
            }
            result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                'Successfully Saved!!', user_access_details);
            res.json({
                'result': result
            })
        })
    },
    getUserAccessDetailsByUserId: function (req, res, next) {

        var result = {};
        var user_id = mongoose.Types.ObjectId(req.params.user_id);
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var lookup = {
            $lookup: {
                from: "oauth_access_tokens",
                localField: "access_token",
                foreignField: "access_token",
                as: "token"
            }
        }
        var unwind_condition = {
            $unwind: "$token"
        }
        var yesterday = moment().format();
        var lookup_array_1 = [
            lookup, unwind_condition,
            {
                $match: {
                    "_user_access_id": user_id
                }
            }
        ]

        UserAccessDetails
            .aggregate(lookup)
            .match({
                "_user_access_id": user_id
            })
            .unwind("$token")
            .exec(function (err, users) {

                if (err) {
                    console.log('Error While fetching AdminUsers: AdminUsersController->getAdminUsers');
                    throw err;
                }

                users = users.filter(function (users) {
                    return (users.token.expires > moment(yesterday));
                })

                // getting first result only
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', users);
                res.json({
                    'result': result
                })
            })
    },
    deleteUserAccessDetailsByUserIdAndAccessToken: function (req, res, next) {

        var result;
        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');
        var user_id = req.params.user_id,
            _access_token = req.body._access_token;
        UserAccessDetails.find({
                $and: [{
                    _user_access_id: user_id
                }, {
                    access_token: _access_token
                }]
            },
            function (err, user_access_details) {
                if (err) {
                    console.log('Error While fetching userAccessDetails: userAccessDetailsController->deleteUserAccessDetailsByUserIdAndAccessToken');
                    throw err;
                }
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', user_access_details);
                res.json({
                    'result': result
                })
            })
    },
}
