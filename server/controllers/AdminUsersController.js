var userDetails = require('./../models').user_details,
    UserAccess = require('./../models').user_access,
    OAuthClients = require('./../models').oauth_clients,
    Notifications = require('./../models').notifications,
    ResultResponses = require('./../helpers/resultResponses'),
    mongoose = require('mongoose'),
    CONSTANTS = require('./../helpers/constants');

module.exports = {

    getAdminUsers: function (req, res) {

        var limit = req.query.limit ? req.query.limit : CONSTANTS.API.DEFAULT_LIMIT;
        var offset = req.query.offset ? req.query.offset : CONSTANTS.API.DEFAULT_OFFSET;
        var data, result = {}, admin_id = new Array(req.member._id);

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var lookup = {
            $lookup: {
                from: "user_details",
                localField: "_id",
                foreignField: "_user_access_id",
                as: "user_details"
            }
        }
        var match = {
            $match: {
                "user_details._user_access_id": {'$nin': admin_id}
            }
        }
        var project = {
            $project: {
                _id: 1,
                created_at: 1,
                email: 1,
                activated: 1,
                country_code: 1,
                contact_number: 1,
                user_details: {
                    _id: 1,
                    first_name: 1,
                    last_name: 1
                }
            }
        }
        var unwind_condition = {
            $unwind: "$user_details"
        }
        var sort_condition = {
            $sort: {
                created_at: -1
            }
        }

        var limit_condition = {
            $limit: parseInt(limit)
        }
        var skip_condition = {
            $skip: parseInt(offset)
        }

        var lookup_array_1 = [
            lookup, match, project, sort_condition, unwind_condition, limit_condition, skip_condition
        ]

        var lookup_array_2 = [
            lookup, project
        ]

        UserAccess.aggregate(lookup_array_1, function (err, users) {

            if (err) {
                console.log('Error While fetching AdminUsers: AdminUsersController->getAdminUsers');
                throw err;
            }

            data = {
                limit: parseInt(limit),
                offset: parseInt(offset)
            }

            UserAccess.aggregate(lookup_array_2, function (err, count) {

                if (err) {
                    console.log('Error While fetching AdminUsers: AdminUsersController->getAdminUsers');
                    throw err;
                }

                data.count = count.length;
                data.users = users;

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Fetched!!', data);

                res.json({
                    'result': result
                })
            })
        });
    },
    postAdminUser: function (req, res, next) {

        var result = {};
        var client_type = parseInt(req.body.client_type);
        var client_name = req.body.client_name;
        var user_id = req.member._id; // using access token
        var redirect = req.body.redirect_url;
        var revoked = req.body.is_revoked;

        OAuthClients.find({
                $or: [{
                    client_name: client_name
                }, {
                    redirect_url: redirect
                }]
            },
            function (err, oauthClients) {

                if (err) {
                    console.log('Error: OAuthUserApplicationsController->postApplication');
                    throw err;
                }

                if (oauthClients.length > 0) {

                    result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.CONFLICT,
                        'Client Already exists!!', null);
                    res.json({
                        'result': result
                    })
                }
                else {

                    var oauthClients = new OAuthClients();
                    oauthClients.client_name = client_name;
                    oauthClients.client_secret = bcrypt.hashSync(randomstring.generate(10), bcrypt.genSaltSync(8), null);
                    oauthClients.redirect_url = redirect;
                    oauthClients.is_revoked = revoked;

                    if (client_type == CONSTANTS.CLIENT_TYPE.PERSONAL_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.PERSONAL_GRANT_TYPE;
                    }
                    else if (client_type == CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.PASSWORD_GRANT_TYPE;
                    }
                    else if (client_type == CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE) {
                        oauthClients.client_type = CONSTANTS.CLIENT_TYPE.AUTH_CODE_GRANT_TYPE;
                    }
                    oauthClients._user_access_id = user_id;
                    oauthClients.save(function (err) {
                        if (err) {
                            console.log('OAuthClients Save Error: OAuthUserApplicationsController->postApplication');
                            throw err;
                        }
                        result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                            'Successfully Created!!', oauthClients);
                        res.json({
                            'result': result
                        })
                    });
                }
            })
    },
    getAdminUserByUserId: function (req, res) {

        var result = {};
        var user_id = mongoose.Types.ObjectId(req.params.user_id);
        console.log(user_id)

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        var lookup = {
            $lookup: {
                from: "user_details",
                localField: "_id",
                foreignField: "_user_access_id",
                as: "user_details"
            }
        }
        var project = {
            $project: {
                _id: 1,
                created_at: 1,
                email: 1,
                activated: 1,
                country_code: 1,
                contact_number: 1,
                user_details: {
                    _id: 1,
                    first_name: 1,
                    last_name: 1,
                    nick_name: 1,
                    sex: 1,
                    cover_pic_updated_at: 1,
                    profile_pic_updated_at: 1,
                    show_dob: 1,
                    dob: 1
                }
            }
        }
        var unwind_condition = {
            $unwind: "$user_details"
        }

        var lookup_array_1 = [
            lookup, project, unwind_condition, {
                $match: {
                    "_id": user_id
                }
            }
        ]

        UserAccess.aggregate(lookup_array_1, function (err, users) {

            if (err) {
                console.log('Error While fetching AdminUsers: AdminUsersController->getAdminUsers');
                throw err;
            }
            // getting first result only
            result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                'Successfully Fetched!!', users[0]);
            res.json({
                'result': result
            })
        });
    },
    putAdminUserInfo: function (req, res) {

        var result = {},
            _user_access_id = req.params.user_id;

        result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
            'Some Error Occurred.');

        userDetails.findOne({
            _user_access_id: _user_access_id
        }, function (err, user) {

            if (err) {
                console.log('Admin User not found Error: AdminUsersController->putAdminUserInfo');
                throw err;
            }
            if (user) {
                user.first_name = req.body.first_name;
                user.last_name = req.body.last_name;
                user.nick_name = req.body.nick_name;
                user.sex = req.body.sex;
                user.show_dob = req.body.show_dob;
                user.dob = new Date(req.body.dob);
                user.save(function (err) {
                    if (err) {
                        console.log('Admin User Save Error: AdminUsersController->putAdminUserInfo');
                        throw err;
                    }
                });
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Details Successfully Updated!!', user);
            }
            else {
                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.UNAUTHORISED,
                    'User not found.!!');
            }
            res.json({
                'result': result
            })
        });

    },
    // Change Contact Number
    putContactNumber: function (req, res, next) {

        var result = {};

        var country_code = req.body.country_code,
            contact_number = req.body.contact_number,
            user_id = req.params.user_id;

        UserAccess.findOne({
            _id: user_id
        }, function (err, userAccess) {

            result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
                'Some Error Occurred.');
            if (err) {
                console.log('Admin User not found Error: AdminUsersController->patchContactNumber');
                throw err;
            }
            if (userAccess) {
                userAccess.country_code = country_code;
                userAccess.contact_number = contact_number;
                userAccess.save(function (err) {
                    if (err) {
                        console.log('Admin User save Error: AdminUsersController->patchContactNumber');
                        throw err;
                    }
                })
                var data = {
                    country_code: country_code,
                    contact_number_updated_at: userAccess.contact_number_updated_at,
                    contact_number: contact_number
                }
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK, 'Phone Number Changed!!', data);
            }
            else {
                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'User Not Found');
            }
            res.json({
                'result': result
            });
        })
    },
    // Change Email Address
    putEmailAddress: function (req, res, next) {

        var result = {};
        var email = req.body.email,
            user_id = req.params.user_id;

        UserAccess.findOne({
            _id: user_id
        }, function (err, userAccess) {

            result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
                'Some Error Occurred.');
            if (err) {
                console.log('Admin User not found Error: AdminUsersController->putEmailAddress');
                throw err;
            }
            if (userAccess) {
                userAccess.email = email;
                userAccess.save(function (err) {
                    if (err) {
                        console.log('Admin User save Error: AdminUsersController->putEmailAddress');
                        throw err;
                    }
                })
                var data = {
                    email: email,
                    email_updated_at: userAccess.email_updated_at
                }
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK, 'Email Address Changed!!', data);
            }
            else {
                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'User Not Found');
            }
            res.json({
                'result': result
            });
        })
    },
    // Change Password
    postChangePassword: function (req, res, next) {

        var result = {};

        var user_id = req.params.user_id;
        var new_password = req.body.new_password;
        var confirm_new_password = req.body.confirm_new_password;

        UserAccess.findOne({
            _id: user_id
        }, function (err, userAccess) {

            result = ResultResponses.failed(CONSTANTS.HTTP_CODES.SERVER_ERROR.INTERNAL_SERVER_ERROR,
                'Some Error Occurred.');
            if (err) {
                console.log('User Not found Error: AdminUsersController->postChangePassword');
                throw err;
            }
            if (userAccess) {

                if (new_password != confirm_new_password) {
                    result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                        'New Password and confirm password are not same.');
                }
                else {
                    userAccess.password = userAccess.generateHash(new_password);
                    userAccess.save(function (err) {
                        if (err) {
                            console.log('Error in Saving new password: AdminUsersController->postChangePassword');
                            throw err;
                        }
                    })
                    result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK, 'Password Changed!!');
                }
            }
            else {
                result = ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'User Not Found');
            }
            res.json({
                'result': result
            });
        })
    }
}
