var Application = require('./../models').application;
// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var Roles = require('./../models').roles;
var User_To_Roles = require('./../models').user_roles;
var ENV = require('./../config/env').environment;
var MongoClient = require('mongodb').MongoClient,
    util = require('util');


module.exports = {

    postInstallApp: function (req, res) {

        var current_version = req.body.current_version;

        var result = {};
        Application.findOne({
            current_version: current_version
        }, function (err, app) {
            if (err) {
                console.log("---- Problem in fetching application: InstallationController -> postInstallApp  -----");
                throw err;
            }

            if (app) {
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST,
                    'Current Version of app is already installed!!', app);

                res.status(209).send({ error: "Current Version of app is already installed!!" });
            } else {

                var new_app = new Application();
                new_app.is_installed = CONSTANTS.APPLICATION_.INSTALLED_.YES;
                new_app.current_version = current_version;
                new_app.save(function (err, app) {
                    if (err) {
                        console.log("---- Problem in Saving new app Version: InstallationController -> postInstallApp -----");
                        throw err;
                    }
                })

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Application is installed!!', new_app);
            }
            res.json({
                'result': result
            })
        })

    },
    checkIfInstalledApp: function (req, res) {

        var result = {};

        Application.findOne({
            current_version: req.params.current_version
        }, function (err, app) {
            if (err) {
                console.log("---- Problem in checking if application is installed -----");
                throw err;
            }

            if (!app) {
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Application is not installed!!', {
                        app: {
                            installed: false
                        }
                    });
            }
            else {

                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Application is installed!!', {
                        app: {
                            installed: true
                        }
                    });
            }
            res.json({
                'result': result
            })
        })
    },
    getCheckDatabaseConnection: function (req, res) {

        var result = {};
        var database_config = util.format('mongodb://%s:%s@%s:%s/%s', ENV.MONGO_.DB_USERNAME, ENV.MONGO_.DB_PASSWORD,
            ENV.MONGO_.DB_HOST, ENV.MONGO_.DB_PORT, ENV.MONGO_.DB_DATABASE_NAME);
        //var database_config =  util.format('mongodb://%s:%s/%s', ENV.MONGO_.DB_HOST, 27017, ENV.MONGO_.DB_DATABASE_NAME);

        MongoClient.connect(database_config, function (err, db) {
            if (err) {
                console.log("----Problem in connection to the database ----");
                throw err;
            } else {
                db.close();
                result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
                    'Successfully Connected to database!!', {
                        database: ENV.MONGO_
                    });
            }
            res.json({
                'result': result
            })
        });
    },
    postHostConnection: function (req, res) {

        var result = ResultResponses.success(CONSTANTS.HTTP_CODES.SUCCESS.OK,
            'Successfully Fetched Server Information!!', {
                "server": ENV.SERVER_
            });
        res.json({
            'result': result
        })
    }
}
