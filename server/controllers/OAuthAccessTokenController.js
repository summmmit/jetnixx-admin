// loading user constants
var CONSTANTS = require('./../helpers/constants');
var ResultResponses = require('./../helpers/resultResponses');
var OAuthClients = require('./../models/oauth_clients');
var bcrypt = require('bcrypt-nodejs');
var randomstring = require("randomstring");
var OAuthAccessToken = require('./../models/oauth_access_tokens');
var OAuthRefreshToken = require('./../models/oauth_refresh_tokens');
var UserAccess = require('./../models/user_access');
var UserDetails = require('./../models/user_details');


module.exports = {

    authenticateAccessTokenClients: function (req, res, next) {

        res.app.oauth.authorise()(req, res, next);
    },
    getUserFromAccessToken: function (req, res, next) {

        var _user_id = req.user.id;
        var member;

        UserAccess.findOne({_id: _user_id}, {
            __v: 0,
            password: 0,
            created_at: 0,
            updated_at: 0,
            deleted_at: 0
        }, function (err, user) {
            if (err || !user) {
                console.log('Error in Finding valid user')
            }
            if (user) {

                member = {
                    _id: user._id,
                    country_code: user.country_code,
                    contact_number: user.contact_number,
                    contact_number_updated_at: user.contact_number_updated_at,
                    primary_email: user.email,
                    primary_email_updated_at: user.email_updated_at,
                    password_updated_at: user.password_updated_at,
                    activated_at: user.activated_at,
                    created_at: user.created_at,
                };

                UserDetails.findOne({_user_access_id: _user_id}, {
                    __v: 0,
                    _user_access_id: 0,
                    created_at: 0,
                    updated_at: 0,
                    deleted_at: 0
                }, function (err, userDetails) {

                    if (err) {
                        console.log('UserDetails Error : authenticated_urls -> middleware')
                        throw err;
                    }

                    if (userDetails) {

                        member.member_details_id = userDetails._id;
                        member.first_name = userDetails.first_name;
                        member.last_name = userDetails.last_name;
                        member.nick_name = userDetails.nick_name;
                        member.sex = userDetails.sex;
                        member.dob = userDetails.dob;
                        member.show_dob = userDetails.show_dob;
                        member.addresses = userDetails.addresses;
                        member.profile_pic = userDetails.profile_pic;
                        member.profile_pic_updated_at = userDetails.profile_pic_updated_at;

                        req.member = member;

                        // return res.status(200).json({result: member})
                        next()

                    } else {
                        return res.status(500).json({result: ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST, 'User not Found!!')})
                    }
                })
            } else {
                return res.status(500).json({result: ResultResponses.invalid(CONSTANTS.HTTP_CODES.CLIENT_ERROR.BAD_REQUEST, 'User not Found!!')})
            }
        })
    }
}