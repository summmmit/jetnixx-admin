var express = require('express'),
    router = express.Router();

module.exports = function (passport) {

    router.use(require('./oauth'));
    router.use(require('./token'));
    router.use(require('./sample'));

    router.use('/api', require('./unAuthenticeAPIs'));
    router.use('/api', require('./authenticeAPIs'));
    return router;
};
