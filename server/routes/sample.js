var express = require('express'),
    router = express.Router();

router.post('/sample', function (req, res, next) {

    console.log(req.body);

    var flash = {
        'status': 'failed',
        'statusCode': 401,
        'statusText': 'Some Error Occurred.'
    }
    res.json({
        'result': flash
    });
});
module.exports = router;
