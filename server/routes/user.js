var userDetailsController = require('./../controllers/UserDetailsController');

module.exports = function (route) {

    route.get('/user', userDetailsController.getUser);
}