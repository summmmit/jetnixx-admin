angular.module('app')

    .constant('AUTH_EVENTS', {
        notAuthenticated: 'auth-not-authenticated'
    })
    .constant('ACTIVITY_TYPES', {
        NO_ACTIVITY: 0,
        SIGN_UP_ACTIVITY: 1,
        DETAILS_UPDATING_ACTIVITY: 2,
    })
    .constant('LOCAL_STORAGE', {
        _KEY_: '_access_token',
        _TIME_: '_expires_at',
        _REFRESH_: '_refresh_token',
    })
    .constant('SESSION_AUTHENTICATION', {
        _ACCESS_TOKEN_EXPIRED_: 1,
        _REFRESH_TOKEN_EXPIRED_: 2,
        _SESSION_EXPIRED_: 3,
        _ACCESS_TOKEN_EXPIRATION_TIME: 1800,
        _REFRESH_TOKEN_EXPIRATION_TIME: 3600
    })
    .constant('IMAGE_TYPE', {
        PROFILE: 'profile',
        COVER: 'cover'
    })
    .constant('API_TYPE', {
        _AUTHENTICATION_: {
            GET_ALL_LABELS: '/api/authentication/labels',
            CHANGE_PASSWORD: '/api/authentication/change/password',
            CHANGE_CONTACT_NUMBER: '/api/user/contact_number',
            CHANGE_EMAIL_ADDRESS: '/api/user/email',
            USER_NOTIFICATION: '/api/user/notifications',
            CHANGE_PROFILE_PIC: '/api/authentication/profile/pic',
            USER_ACCESS_DETAILS: '/api/user_access_details'
        },
        _MEMBERSHIP_: {
            OAUTH_TOKEN: '/oauth/token',
            USER: '/api/user',
            SIGN_UP: '/api/user',
            LOG_IN: '/api/membership/login',
            LOG_OUT: '/authentication/logout',
            PASSWORD_AUTHENTICATION: '/api/user/authenticate_password',
            USER_STATUS: '/authentication/user/status',
            USER_ACTIVATION: '/authentication/activate/',
            CHECK_IF_USER_EXISTS: '/authentication/user/',
            RESET_PASSWORD: '/authentication/reset/',
            CHANGE_PASSWORD: '/authentication/change/password',
            APPLICATIONS: '/api/applications',
        },
        _ACTIVITIES_: {
            FETCH: '/api/authentication/activities',
        },
        _LABELS_: {
            FETCH: '/api/authentication/labels',
        },
        _NOTIFICATIONS_: {
            FETCH: '/api/authentication/notifications',
        },
        _CONTACTS_: {
            PHONE_NUMBERS: '/api/authentication/numbers',
        },
        _ADMIN_: {
            CLIENTS: '/oauth/clients',
            ROLES: '/api/user/roles',
            PERMISSIONS: '/api/admin/permission',
            ASSIGN_ROLE_TO_PERMISSIONS: '/api/assign/admin/permission',
            USERS: '/api/admin/user',
            USER_SESSIONS: '/api/admin/user/sessions',
            USER_STATUS: '/api/admin/user/status',
            CONTACT_NUMBER: '/api/admin/user/contact_number',
            EMAIL: '/api/admin/user/email',
            PASSWORD: '/api/admin/user/password',
            DATABASE: '/api/database_connection',
            SERVER: '/api/server_connection',
            APP_INSTALL: '/api/install_app'
        }
    })
    .constant('RESULT_RESPONSE_TYPE', {
        SUCCESS: 'success',
        INVALID: 'invalid',
        FAILED: 'failed',
    })
    .constant('OS_TYPE', {
        WEB_BROWSER: 1,
        ANDROID: 2,
        IOS: 3,
        UNKNOWN: 9,
    })
    .constant('HTTP_CODES', {
        SUCCESS: {
            OK: 200,
            NON_AUTHORITATIVE_INFORMATION: 203,
            NO_CONSENT: 204,
            ALREADY_REPORTED: 208
        },
        CLIENT_ERROR: {
            BAD_REQUEST: 400,
            UNAUTHORISED: 401,
            FORBIDDEN: 403,
            NOT_FOUND: 404,
            NOT_ACCEPTABLE: 406,
            CONFLICT: 409,
        },
        SERVER_ERROR: {
            INTERNAL_SERVER_ERROR: 500
        }
    })
    .constant('FRIENDSHIP', {
        STATUS: {
            REQUEST_SEND: 1,
            REQUEST_PENDING: 2,
            REQUEST_ACCEPTED: 3,
            REQUEST_NOT_SEND: 4,
        },
        STATUS_STRING: {
            REQUEST_SEND: 'Request Send',
            REQUEST_PENDING: 'Pending',
            REQUEST_ACCEPTED: 'Accepted',
            REQUEST_NOT_SEND: 'Add Friend',
        }
    });
