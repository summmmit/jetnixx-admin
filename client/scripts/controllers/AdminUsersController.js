app.controller('AdminUsersController',
['$scope', '$stateParams', '$timeout', 'AdminUsersService', '$mdDialog', 'showToastService', 'HTTP_CODES', '$filter', 'AuthService',
    function ($scope, $stateParams, $timeout, AdminUsersService, $mdDialog, showToastService, HTTP_CODES, $filter, AuthService) {

        AdminUsersService.getAllAdminUsers()
            .then(function (response) {
                $scope.users = response.users;
                $scope.total_users = response.count;
                $timeout(function(){
                    $('.table').trigger('footable_redraw');
                }, 100);
            })

        $scope.newUserDialog = function (ev) {
            $mdDialog.show({
                controller: createNewUserController,
                templateUrl: 'views/application/admin/partials/new_user.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (user) {
                    AuthService.register(user)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                var new_user = {
                                    user_details: {
                                        first_name: user.first_name,
                                        last_name: user.last_name,
                                    },
                                    _id: response.data.user._id,
                                    activated: response.data.user.activated,
                                    contact_number: response.data.user.contact_number,
                                    country_code: response.data.user.country_code,
                                    email: response.data.user.email,
                                    created_at: new Date()
                                }
                                $scope.users.push(new_user)
                                showToastService.showSimpleToast('SuccessFully Created!!');
                                $timeout(function(){
                                    $('.table').trigger('footable_redraw');
                                }, 100);
                            } else {
                                showToastService.showSimpleToast('Problem in Creating Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };
        function createNewUserController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createUser = function (user) {
                $mdDialog.hide(user);
            };
        }
        $scope.changeUserStatus = function (status) {
            console.log(status)
        }
    }]
);

app.filter('UserStatus', function () {
    return function (status) {

        if (status == 0) {
            return 'Not Activated';
        } else if (status == 1) {
            return 'Active';
        } else if (status == 2) {
            return 'Disabled';
        } else if (status == 3) {
            return 'Suspended';
        }
    }
});