app.controller('AdminDashboardController', ['$scope', '$timeout', 'AdminUsersService', '$mdDialog', 'showToastService', 'HTTP_CODES', '$filter', 'ClientsService',
    function ($scope, $timeout, AdminUsersService, $mdDialog, showToastService, HTTP_CODES, $filter, ClientsService) {

        AdminUsersService.getAllAdminUsers()
            .then(function (response) {
                $scope.total_users = response.count;
            })

        ClientsService.getAllClients()
            .then(function (response) {
                $scope.total_applications = response.count;
            })

    }]
);