app.controller('AdminRolesController', ['$scope', '$timeout', 'AdminRolesService', '$mdDialog', 'showToastService', 'HTTP_CODES', '$filter',
    function ($scope, $timeout, AdminRolesService, $mdDialog, showToastService, HTTP_CODES, $filter) {

        AdminRolesService.getAllRoles()
            .then(function (response) {
                $scope.Roles = response.Roles;
            })


        $scope.viewRoleDialog = function (ev) {
            $mdDialog.show({
                templateUrl: 'views/application/admin/partials/role_view.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    role: ev
                },
                controller: viewRoleController
            })
                .then(function (role) {

                    AdminRolesService.putRoles(role)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                //$scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully edited Application!!');
                            } else {
                                showToastService.showSimpleToast('Problem in edited Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };
        function viewRoleController($scope, $mdDialog, role) {
            $scope.role = role;
            $scope.edit = true;
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createRole = function (role) {
                $mdDialog.hide(role);
            };
        }
        $scope.editRoleDialog = function (ev) {
            $mdDialog.show({
                templateUrl: 'views/application/admin/partials/new_role.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen, // Only for -xs, -sm breakpoints.
                locals: {
                    role: ev
                },
                controller: editRoleController
            })
                .then(function (role) {

                    AdminRolesService.putRoles(role)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                //$scope.clients.push(response.data);
                                showToastService.showSimpleToast('SuccessFully edited Application!!');
                            } else {
                                showToastService.showSimpleToast('Problem in edited Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };
        function editRoleController($scope, $mdDialog, role) {
            $scope.role = role;
            $scope.edit = true;
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createRole = function (role) {
                $mdDialog.hide(role);
            };
        }
        $scope.deleteRole = function (role) {
            AdminRolesService.deleteRoles(role)
                .then(function (response) {
                    if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                        var foundItem = $filter('filter')($scope.Roles, {_id: response.data._id}, true)[0];
                        var index = $scope.Roles.indexOf(foundItem);
                        $scope.Roles.splice(index, 1);
                        showToastService.showSimpleToast('SuccessFully Deleted!!');
                    } else {
                        showToastService.showSimpleToast('Problem in Creating right now!!');
                    }
                });
        }
        $scope.newRoleDialog = function (ev) {
            $mdDialog.show({
                controller: createNewRoleController,
                templateUrl: 'views/application/admin/partials/new_role.html',
                parent: angular.element(document.body),
                targetEvent: ev,
                clickOutsideToClose: true,
                fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            })
                .then(function (role) {
                    AdminRolesService.postRoles(role)
                        .then(function (response) {
                            if (response.statusCode == HTTP_CODES.SUCCESS.OK) {
                                var new_role = response.data;
                                new_role.user_role = [];
                                $scope.Roles.push(new_role);
                                showToastService.showSimpleToast('SuccessFully Created!!');
                            } else {
                                showToastService.showSimpleToast('Problem in Creating Application right now!!');
                            }
                        });
                }, function (response) {
                    // cancelled dialog
                });
        };
        function createNewRoleController($scope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };

            $scope.cancel = function () {
                $mdDialog.cancel();
            };

            $scope.createRole = function (role) {
                $mdDialog.hide(role);
            };
        }
    }
]);