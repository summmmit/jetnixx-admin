'use strict';

/**
 * @ngdoc function
 * @name app.config:uiRouter
 * @description
 * # Config
 * Config for the router
 */
angular.module('app')
    .run(
        ['$rootScope', '$state', '$stateParams', 'AuthService', 'InstallationService', 'UserPersistenceService', 'SESSION_AUTHENTICATION',
            function ($rootScope, $state, $stateParams, AuthService, InstallationService, UserPersistenceService, SESSION_AUTHENTICATION) {
                // check if the user is logged in and page is restricted without login
                $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
                    if (toState.data.restricted == true && AuthService.isAuthenticated() == false) {
                        e.preventDefault();
                        var at_expiration_time = UserPersistenceService.getTokenExpirationTime(),
                            now = moment().unix(),
                            rt_expiration_time = parseInt(SESSION_AUTHENTICATION._REFRESH_TOKEN_EXPIRATION_TIME) + parseInt(at_expiration_time);

                        if (at_expiration_time) {
                            if (now > at_expiration_time && now < rt_expiration_time) {
                                $state.go('authentication.re_signin');
                            }
                            else if (now > rt_expiration_time) {
                                AuthService.logout();
                                $state.go('authentication.signin', {
                                    alertParam: {
                                        'statusText': 'Please Login again!!'
                                    }
                                });
                            }
                        }
                        else {
                            $state.go('authentication.signin');
                        }
                    }
                    else if (toState.data.restricted == false && AuthService.isAuthenticated() == true) {
                        e.preventDefault();
                        $state.go('page.home');
                    }
                    else if (toState.data.restricted == true && AuthService.isAuthenticated() == true) {
                        UserPersistenceService.setTokenExpirationTime()
                    }
                });
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
            }
        ]
    )
    .config(
        ['$stateProvider', '$urlRouterProvider', 'MODULE_CONFIG',
            function ($stateProvider, $urlRouterProvider, MODULE_CONFIG) {
                var p = getParams('layout'),
                    l = p ? p + '.' : '',
                    layout = 'views/layout.' + l + 'html',
                    aside = 'views/aside.' + l + 'html',
                    content = 'views/content.' + l + 'html';

                $urlRouterProvider
                    .otherwise('/page/home');
                $stateProvider
                // authentication pages -----------------------------------------------------------------------------------Start
                    .state('authentication', {
                        url: '/authentication',
                        templateUrl: 'views/authentication/authentication_template.html'
                    })
                    .state('authentication.installation_database', {
                        url: '/installation/database',
                        params: {
                            alertParam: null,
                            installation_step: 1
                        },
                        templateUrl: 'views/authentication/installation_database.html',
                        controller: 'InstallationController',
                        resolve: load(['scripts/services/InstallationService.js', 'scripts/controllers/InstallationController.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.installation_server', {
                        url: '/installation/server',
                        params: {
                            alertParam: null,
                            installation_step: 2
                        },
                        templateUrl: 'views/authentication/installation_server.html',
                        controller: 'InstallationController',
                        resolve: load(['scripts/services/InstallationService.js', 'scripts/controllers/InstallationController.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.installation_app', {
                        url: '/installation/app',
                        params: {
                            alertParam: null,
                            installation_step: 3
                        },
                        templateUrl: 'views/authentication/installation_app.html',
                        controller: 'InstallationController',
                        resolve: load(['scripts/services/InstallationService.js', 'scripts/controllers/InstallationController.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.signin', {
                        url: '/signin',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/signin.html',
                        controller: 'AuthenticationController',
                        resolve: load(['scripts/controllers/InstallationController.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.activation', {
                        url: '/activate/:user_id/:activation_code',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/activation.html',
                        controller: 'activationController',
                        resolve: load(['scripts/controllers/activationController.js', 'scripts/services/authenticationService.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.resetPassword', {
                        url: '/reset/:user_id/:reset_code',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/activation.html',
                        controller: 'resetPasswordController',
                        resolve: load(['scripts/controllers/activationController.js', 'scripts/services/authenticationService.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.signup', {
                        url: '/signup',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/signup.html',
                        controller: 'AuthenticationController',
                        resolve: load(['scripts/controllers/AuthenticationController.js', 'scripts/services/AuthService.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.forgot-password', {
                        url: '/forgot-password',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/forgot-password.html',
                        controller: 'AuthenticationController',
                        resolve: load(['scripts/controllers/AuthenticationController.js', 'scripts/services/authenticationService.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.change-password', {
                        url: '/change-password/:user_id',
                        params: {
                            alertParam: null,
                            userParam: null
                        },
                        templateUrl: 'views/authentication/change-password.html',
                        controller: 'AuthenticationController',
                        resolve: load(['scripts/controllers/AuthenticationController.js',
                        'scripts/services/authenticationService.js']),
                        data: {
                            restricted: false
                        }
                    })
                    .state('authentication.re_signin', {
                        url: '/relogin',
                        params: {
                            alertParam: null
                        },
                        templateUrl: 'views/authentication/relogin.html',
                        controller: 'AuthenticationReLoginController',
                        resolve: load(['scripts/controllers/AuthenticationReLoginController.js']),
                        data: {
                            restricted: false
                        }
                    })
                    // authentication pages -----------------------------------------------------------------------------------END
                    .state('page', {
                        url: '/page',
                        controller: 'HomeController',
                        resolve: load(['scripts/controllers/HomeController.js', 'scripts/services/HomeService.js']),
                        views: {
                            '': {
                                templateUrl: layout
                            },
                            'aside': {
                                templateUrl: aside
                            },
                            'content': {
                                templateUrl: content
                            }
                        }
                    })
                    .state('page.blank', {
                        url: '/blank',
                        templateUrl: 'views/pages/blank.html',
                        data: {
                            title: 'Blank',
                            restricted: true
                        }
                    })
                    .state('page.home', {
                        url: '/home',
                        params: {
                            userParam: null
                        },
                        templateUrl: 'views/pages/home.html',
                        controller: 'AuthenticationController',
                        resolve: load(['scripts/controllers/AuthenticationController.js', 'scripts/services/authenticationService.js']),
                        data: {
                            title: 'Home',
                            restricted: true
                        }
                    })
                    .state('page.pc', {
                        url: '/pc',
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    .state('page.sp', {
                        url: '/sp',
                        abstract: true,
                        template: '<div ui-view></div>'
                    })
                    // Profile Page ------------------------------------------------------------------------------------Start
                    .state('page.pc.profile', { // for pc
                        url: '/profile',
                        resolve: load([
                            'scripts/controllers/ProfileController.js',
                            'scripts/controllers/ActivitiesController.js',
                            'scripts/controllers/material.js',
                            'scripts/factories/ActivitiesFactory.js',
                            'scripts/filters/PhoneNumberFilters.js',
                            'moment'
                        ]),
                        views: {
                            '': {
                                templateUrl: 'views/pages/profile.html',
                                controller: 'ProfileController',
                            },
                            'profile_user_card@': {
                                templateUrl: 'views/pages/partials/profile_user_card.html',
                                controller: 'ProfileController'
                            },
                            'activities_card@': {
                                templateUrl: 'views/pages/partials/activities_card.html',
                                controller: 'ActivitiesController'
                            }
                        },
                        data: {
                            title: 'Profile',
                            folded: true,
                            restricted: true
                        }
                    })
                    .state('page.sp.profile', { // for sp
                        url: '/profile',
                        resolve: load([
                            'scripts/controllers/ProfileController.js',
                            'scripts/controllers/material.js',
                            'scripts/factories/ActivitiesFactory.js',
                            'scripts/filters/PhoneNumberFilters.js',
                            'moment'
                        ]),
                        views: {
                            '': {
                                templateUrl: 'views/pages/profile.html',
                                controller: 'ProfileController',
                            },
                            'profile_user_card@': {
                                templateUrl: 'views/pages/partials/profile_user_card.html',
                                controller: 'ProfileController'
                            },
                        },
                        data: {
                            title: 'Profile',
                            folded: true,
                            restricted: true
                        }
                    })
                    // Settings Page ------------------------------------------------------------------------------------Start
                    .state('page.pc.settings', { // for pc
                        url: '/settings',
                        resolve: load([
                            'scripts/controllers/material.js',
                            'scripts/services/HomeService.js',
                            'ngImgCrop',
                            'scripts/controllers/SettingsController.js',
                            'scripts/services/SettingsService.js'
                        ]),
                        views: {
                            '': {
                                templateUrl: 'views/pages/settings.html',
                                controller: 'SettingsController',
                            },
                            'settings_user_card@': {
                                templateUrl: 'views/pages/partials/settings_user_card.html',
                                controller: 'SettingsController'
                            },
                            'settings_panel_pc@': {
                                templateUrl: 'views/pages/partials/settings_panel_pc.html',
                            },
                            'settings_panel_navigation_card_pc@': {
                                templateUrl: 'views/pages/partials/settings_panel_navigation_card_pc.html',
                                controller: 'SettingsController'
                            }
                        },
                        data: {
                            title: 'Settings',
                            restricted: true,
                            folded: true
                        }
                    })
                    .state('page.sp.settings', { // for pc
                        url: '/settings',
                        resolve: load([
                            'scripts/controllers/material.js',
                            'scripts/services/HomeService.js',
                            'ngImgCrop',
                            'scripts/controllers/SettingsController.js',
                            'scripts/services/SettingsService.js'
                        ]),
                        views: {
                            '': {
                                templateUrl: 'views/pages/settings.html',
                                controller: 'SettingsController',
                            },
                            'settings_user_card@': {
                                templateUrl: 'views/pages/partials/settings_user_card.html',
                                controller: 'SettingsController'
                            },
                            'settings_panel_navigation_card_pc@': {
                                templateUrl: 'views/pages/partials/settings_panel_navigation_card_sp.html',
                                controller: 'SettingsController'
                            }
                        },
                        data: {
                            title: 'Settings',
                            restricted: true,
                            folded: true
                        }
                    })
                    .state('page.pc.settings.public', {
                        url: '/public',
                        controller: 'PublicSettingsController',
                        templateUrl: 'views/pages/partials/settings/public_settings.html',
                        resolve: load(['scripts/controllers/PublicSettingsController.js',
                            'scripts/services/ShowToastService.js', ]),
                        data: {
                            child: true,
                            restricted: true
                        }
                    })
                    .state('page.pc.settings.account', {
                        url: '/account',
                        controller: 'AccountSettingsController',
                        templateUrl: 'views/pages/partials/settings/account_settings.html',
                        resolve: load(['scripts/controllers/AccountSettingsController.js',
                            'scripts/services/ShowToastService.js',
                            'scripts/filters/PhoneNumberFilters.js']),
                        data: {
                            child: true,
                            restricted: true
                        }
                    })
                    .state('page.pc.settings.notifications', {
                        url: '/notifications',
                        controller: 'NotificationsSettingsController',
                        templateUrl: 'views/pages/partials/settings/notifications_settings.html',
                        resolve: load(['scripts/controllers/NotificationsSettingsController.js', 'scripts/services/ShowToastService.js']),
                        data: {
                            restricted: true,
                            child: true
                        }
                    })
                    // Settings Page ------------------------------------------------------------------------------------Start
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        controller: 'HomeController',
                        resolve: load(['scripts/controllers/HomeController.js', 'scripts/services/HomeService.js']),
                        views: {
                            '': {
                                templateUrl: layout
                            },
                            'aside': {
                                templateUrl: aside
                            },
                            'content': {
                                templateUrl: content
                            }
                        }
                    })
                    // Settings for sp -----------------------
                    .state('app.sp', {
                        url: '/sp',
                        controller: 'SettingsController',
                        templateUrl: 'views/pages/settings.html',
                        resolve: load([
                            'scripts/controllers/material.js',
                            'scripts/services/HomeService.js',
                            'ngImgCrop',
                            'scripts/controllers/SettingsController.js',
                            'scripts/services/SettingsService.js']),
                        data: {
                            title: 'Settings',
                            restricted: true,
                            folded: true
                        }
                    })
                    .state('app.sp.settings', {
                        url: '/settings',
                        controller: 'SettingsController',
                        templateUrl: 'views/pages/profile.html',
                        resolve: load([
                            'scripts/controllers/material.js',
                            'scripts/services/HomeService.js',
                            'ngImgCrop',
                            'scripts/controllers/SettingsController.js',
                            'scripts/services/SettingsService.js']),
                        data: {
                            title: 'Settings',
                            restricted: true,
                            folded: true
                        }
                    })
                    .state('app.sp.public', {
                        url: '/public',
                        templateUrl: 'views/pages/partials/settings/public_settings.html',
                        controller: 'PublicSettingsController',
                        resolve: load(['scripts/controllers/PublicSettingsController.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            restricted: true,
                            child: true
                        }
                    })
                    .state('app.sp.account', {
                        url: '/account',
                        controller: 'AccountSettingsController',
                        templateUrl: 'views/pages/partials/settings/account_settings.html',
                        resolve: load(['scripts/controllers/AccountSettingsController.js',
                            'scripts/services/ShowToastService.js',
                            'scripts/filters/PhoneNumberFilters.js']),
                        data: {
                            child: true,
                            restricted: true
                        }
                    })
                    .state('app.sp.notifications', {
                        url: '/notifications',
                        controller: 'NotificationsSettingsController',
                        templateUrl: 'views/pages/partials/settings/notifications_settings.html',
                        resolve: load(['scripts/controllers/NotificationsSettingsController.js', 'scripts/services/ShowToastService.js']),
                        data: {
                            restricted: true,
                            child: true
                        }
                    })
                    .state('app.activities', { //---------------activities page
                        url: '/activities',
                        templateUrl: 'views/pages/activities.html',
                        controller: 'ActivitiesController',
                        resolve: load(['scripts/controllers/material.js',
                            'scripts/controllers/ActivitiesController.js',
                            'scripts/factories/ActivitiesFactory.js',
                            'scripts/filters/PhoneNumberFilters.js',
                            'moment']),
                        data: {
                            title: 'Your Activities',
                            folded: true,
                            restricted: true
                        }
                    })
                    // Settings Page ------------------------------------------------------------------------------------Start
                    .state('app.settings', {
                        url: '/settings',
                        controller: 'SettingsController',
                        templateUrl: 'views/pages/settings.html',
                        resolve: load([
                            'scripts/controllers/material.js',
                            'scripts/services/HomeService.js',
                            'ngImgCrop',
                            'scripts/controllers/SettingsController.js',
                            'scripts/services/SettingsService.js']),
                        data: {
                            title: 'Settings',
                            restricted: true,
                            folded: true
                        }
                    })
                    .state('app.settings.public', {
                        url: '/public',
                        controller: 'PublicSettingsController',
                        templateUrl: 'views/pages/partials/settings/public_settings.html',
                        resolve: load(['scripts/controllers/PublicSettingsController.js',
                            'scripts/services/ShowToastService.js', ]),
                        data: {
                            title: 'Public Settings',
                            restricted: true
                        }
                    })
                    .state('app.settings.account', {
                        url: '/account',
                        controller: 'AccountSettingsController',
                        templateUrl: 'views/pages/partials/settings/account_settings.html',
                        resolve: load(['scripts/controllers/AccountSettingsController.js',
                            'scripts/services/ShowToastService.js',
                            'scripts/filters/PhoneNumberFilters.js', ]),
                        data: {
                            title: 'Account Settings',
                            restricted: true
                        }
                    })
                    .state('app.settings.notifications', {
                        url: '/notifications',
                        controller: 'NotificationsSettingsController',
                        templateUrl: 'views/pages/partials/settings/notifications_settings.html',
                        resolve: load(['scripts/controllers/NotificationsSettingsController.js', 'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'Notifications Settings',
                            restricted: true
                        }
                    })
                    // not used still
                    .state('app.settings.profile', {
                        url: '/profile',
                        controller: 'ImageCropController',
                        templateUrl: 'views/pages/partials/settings/image_crop.html',
                        resolve: load(['scripts/controllers/ImageCropController.js',
                            'scripts/services/ShowToastService.js',
                            'scripts/services/HomeService.js',
                            'bower_components/ng-file-upload/ng-file-upload.min.js',
                            'bower_components/ng-file-upload/ng-file-upload-shim.min.js', ]),
                        data: {
                            title: 'Profile Pic',
                            restricted: true,
                            child: true
                        }
                    })
                    // settings ------------------------------------------------------------------------------------Finish
                    .state('app.contacts', {
                        url: '/contacts',
                        templateUrl: 'views/application/contacts/index.html',
                        data: {
                            title: 'List',
                            restricted: true
                        }
                    })
                    .state('app.contactslist', {
                        url: '/contacts/list',
                        controller: 'contactsController',
                        templateUrl: 'views/application/contacts/list.html',
                        resolve: load(['scripts/controllers/contactsController.js']),
                        data: {
                            title: 'Contact List',
                            restricted: true
                        }
                    })

                // SNS APP-----------------------------------------------------------------------------------START
                .state('sns', {
                        url: '/sns',
                        abstract: true,
                        resolve: load(['scripts/controllers/HomeController.js', 'scripts/services/HomeService.js']),
                        views: {
                            '': {
                                templateUrl: layout
                            },
                            'aside': {
                                templateUrl: aside
                            },
                            'content': {
                                templateUrl: content
                            }
                        },
                        data: {
                            folded: true
                        }
                    })
                    .state('sns.profile', {
                        url: '/{user_id}',
                        templateUrl: 'views/application/sns/profile.html',
                        controller: 'SNSProfileController',
                        resolve: load(['scripts/controllers/application/sns/SNSProfileController.js',
                            'scripts/filters/OtherUserFriendShipStatusFilter.js']),
                        data: {
                            title: 'Profile',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })

                // Admin APP-----------------------------------------------------------------------------------START
                .state('admin', {
                        url: '/admin',
                        abstract: true,
                        resolve: load([
                            'scripts/controllers/HomeController.js',
                            'scripts/services/HomeService.js'
                        ]),
                        views: {
                            '': {
                                templateUrl: layout
                            },
                            'aside': {
                                templateUrl: aside
                            },
                            'content': {
                                templateUrl: content
                            }
                        },
                        data: {
                            folded: true
                        }
                    })
                    .state('admin.clients', {
                        url: '/clients',
                        templateUrl: 'views/application/admin/clients.html',
                        controller: 'ClientsController',
                        resolve: load(['scripts/controllers/ClientsController.js',
                            'xeditable', 'scripts/controllers/bootstrap.js',
                            'scripts/services/ClientsService.js', 'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'Applications',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.dashboard', {
                        url: '/dashboard',
                        templateUrl: 'views/application/admin/dashboard.html',
                        controller: 'AdminDashboardController',
                        resolve: load(['scripts/controllers/AdminDashboardController.js',
                            'xeditable', 'scripts/controllers/bootstrap.js',
                            'scripts/services/AdminUsersService.js',
                            'scripts/services/ClientsService.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'DashBoard',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.roles', {
                        url: '/roles',
                        templateUrl: 'views/application/admin/roles.html',
                        controller: 'AdminRolesController',
                        resolve: load(['scripts/controllers/AdminRolesController.js',
                            'xeditable', 'scripts/controllers/bootstrap.js',
                            'scripts/services/AdminRolesService.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'Roles',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.permissions', {
                        url: '/permissions',
                        templateUrl: 'views/application/admin/permissions.html',
                        controller: 'AdminPermissionsController',
                        resolve: load(['scripts/controllers/AdminPermissionsController.js',
                            'xeditable', 'scripts/controllers/bootstrap.js',
                            'scripts/services/AdminPermissionsService.js',
                            'scripts/services/AdminRolesService.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'Permissions',
                            restricted: true,
                            theme: {
                                primary: 'blue'
                            }
                        }
                    })
                    .state('admin.users', {
                        url: '/users',
                        templateUrl: 'views/application/admin/users.html',
                        controller: 'AdminUsersController',
                        resolve: load(['scripts/controllers/AdminUsersController.js',
                            'xeditable',
                            'scripts/controllers/bootstrap.js',
                            'scripts/services/ClientsService.js',
                            'scripts/services/AdminUsersService.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'Users',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.users_profile', {
                        url: '/user-profile/:user_id',
                        templateUrl: 'views/application/admin/user-profile.html',
                        controller: 'AdminUsersProfileController',
                        resolve: load(['scripts/controllers/AdminUsersProfileController.js',
                            'xeditable',
                            'scripts/controllers/bootstrap.js',
                            'scripts/services/ClientsService.js',
                            'scripts/services/AdminUsersService.js',
                            'scripts/filters/PhoneNumberFilters.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'User Profile',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.users_settings', {
                        url: '/users-settings/:user_id',
                        templateUrl: 'views/application/admin/user-settings.html',
                        controller: 'AdminUsersProfileController',
                        resolve: load(['scripts/controllers/AdminUsersProfileController.js',
                            'xeditable',
                            'moment',
                            'scripts/controllers/bootstrap.js',
                            'scripts/services/AdminUsersService.js',
                            'scripts/services/ClientsService.js',
                            'scripts/filters/PhoneNumberFilters.js',
                            'scripts/services/ShowToastService.js']),
                        data: {
                            title: 'User Settings',
                            restricted: true,
                            theme: {
                                primary: 'green'
                            }
                        }
                    })
                    .state('admin.users_settings.public', {
                        url: '/public',
                        templateUrl: 'views/application/admin/partials/user_public_settings.html'
                    })
                    .state('admin.users_settings.account', {
                        url: '/account',
                        templateUrl: 'views/application/admin/partials/user_account_settings.html'
                    });

                function load(srcs, callback) {
                    return {
                        deps: ['$ocLazyLoad', '$q',
                            function ($ocLazyLoad, $q) {
                                var deferred = $q.defer();
                                var promise = false;
                                srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                                if (!promise) {
                                    promise = deferred.promise;
                                }
                                angular.forEach(srcs, function (src) {
                                    promise = promise.then(function () {
                                        angular.forEach(MODULE_CONFIG, function (module) {
                                            if (module.name == src) {
                                                if (!module.module) {
                                                    name = module.files;
                                                }
                                                else {
                                                    name = module.name;
                                                }
                                            }
                                            else {
                                                name = src;
                                            }
                                        });
                                        return $ocLazyLoad.load(name);
                                    });
                                });
                                deferred.resolve();
                                return callback ? promise.then(function () {
                                    return callback();
                                }) : promise;
                            }]
                    }
                }

                function getParams(name) {
                    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                        results = regex.exec(location.search);
                    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
                }
            }
        ]
    );
