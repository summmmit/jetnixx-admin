// persisting email as _user in the cookies for the user to identify in the sessions.
app.service('AdminPermissionsService', ['$http', '$q', 'API_TYPE', 'GetURLFactory',
    function ($http, $q, API_TYPE, GetURLFactory) {

        return {
            getOnePermission: function (permission) {

                var defer = $q.defer();
                $http.delete(GetURLFactory.getURL() + API_TYPE._ADMIN_.PERMISSIONS + '/' + permission._id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            getAllPermissions: function (limit, offset) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.PERMISSIONS, {
                    params: {
                        limit: limit,
                        offset: offset
                    }
                })
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            putPermissions: function (permission) {

                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.PERMISSIONS + '/' + permission._id, permission)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            postPermissions: function (permission) {
                var defer = $q.defer();
                $http.post(GetURLFactory.getURL() + API_TYPE._ADMIN_.PERMISSIONS, permission)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            deletePermissions: function (permission) {

                var defer = $q.defer();
                $http.delete(GetURLFactory.getURL() + API_TYPE._ADMIN_.PERMISSIONS + '/' + permission._id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            postAssignPermissionToRole: function (role, permission) {
                var role_to_permission = {
                    role_id: role._id,
                    permission_id: permission._id,
                }
                var defer = $q.defer();
                $http.post(GetURLFactory.getURL() + API_TYPE._ADMIN_.ASSIGN_ROLE_TO_PERMISSIONS, role_to_permission)
                    .then(
                        // success
                        function (response) {
                            console.log(response)
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            }
        }
    }
])
