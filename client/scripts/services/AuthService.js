'use strict';

app.service('AuthService', ['$http', '$q', 'UserPersistenceService', 'IdentifyDeviceTypeFactory', 'HTTP_CODES', 'API_TYPE', 'envService', 'APP_INFO_',
    function ($http, $q, UserPersistenceService, IdentifyDeviceTypeFactory, HTTP_CODES, API_TYPE, envService, APP_INFO_) {

        var API_URL = envService.read('API_URL');

        UserPersistenceService.loadUserCredentials();

        return {
            login: function (member) {

                var defer = $q.defer();
                member.client_id = APP_INFO_.CLIENT_INFO.CLIENT_ID;
                member.client_secret = APP_INFO_.CLIENT_INFO.CLIENT_SECRET;
                member.grant_type = "password";

                $http.post(API_URL + API_TYPE._MEMBERSHIP_.OAUTH_TOKEN, member)
                    .then(
                        // success
                        function (response) {
                            if (response.status == HTTP_CODES.SUCCESS.OK) {
                                UserPersistenceService.storeUserCredentials(response.data);
                            }
                            defer.resolve(response);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            register: function (user) {

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._MEMBERSHIP_.USER, user)
                    .then(
                        function (response) {
                            defer.resolve(response.data.result);
                        },
                        function (response) {
                            reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            logout: function () {
                UserPersistenceService.destroyUserCredentials();
            },
            getMemberInfo: function () {

                var defer = $q.defer();
                $http.get(API_URL + API_TYPE._AUTHENTICATION_.USER)
                    .then(
                        function (response) {
                            defer.resolve(response.data.result);
                        },
                        function (response) {
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            isAuthenticated: function () {
                return UserPersistenceService.isAuthenticated();
            },
            reAuthenticateUser: function (relogin) {
                // get refresh token
                // get access token from refresh token

                var defer = $q.defer();
                var member = {
                    email: relogin.email,
                    password: relogin.password
                }
                var token = {

                    'client_id': APP_INFO_.CLIENT_INFO.CLIENT_ID,
                    'client_secret': APP_INFO_.CLIENT_INFO.CLIENT_SECRET,
                    'grant_type': 'refresh_token',
                    'refresh_token': relogin.refresh_token
                };

                $http.post(API_URL + API_TYPE._MEMBERSHIP_.PASSWORD_AUTHENTICATION, member)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                return $http.post(API_URL + API_TYPE._MEMBERSHIP_.OAUTH_TOKEN, token);
                            } else {
                                // send notification
                                defer.resolve(response.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            defer.reject(response);
                        }
                    )
                    .then(
                        // success
                        function (response) {
                            if (response.status == 200) {
                                UserPersistenceService.storeUserCredentials(response.data);
                            }
                            defer.resolve(response);
                        },
                        // failed
                        function (response) {
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            getBrowserDetails: function () {

                $http.get("https://jsonip.com")
                    .then(
                        function (response) {
                            console.info(response)
                        },
                        function (response) {
                        }
                    );

                delete $httpProvider.defaults.headers.common['X-Requested-With'];

            },
            postUserAccessDetails: function (user_access_details) {

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._AUTHENTICATION_.USER_ACCESS_DETAILS, user_access_details)
                    .then(
                        // success
                        function (response) {
                            if (response.status == HTTP_CODES.SUCCESS.OK) {

                            }
                            defer.resolve(response);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
        };
    }
])


app.factory('AuthInterceptor', function ($rootScope, $q, AUTH_EVENTS) {
    return {
        responseError: function (response) {
            $rootScope.$broadcast({
                401: AUTH_EVENTS.notAuthenticated,
            }[response.status], response);
            return $q.reject(response);
        }
    };
})

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('AuthInterceptor');
});
