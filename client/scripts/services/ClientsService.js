// persisting email as _user in the cookies for the user to identify in the sessions.
app.service('ClientsService', ['$http', '$q', 'API_TYPE', 'GetURLFactory',
    function ($http, $q, API_TYPE, GetURLFactory) {

        return {
            getAllClients: function (limit, offset) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.CLIENTS, {
                    params: {
                        limit: limit,
                        offset: offset
                    }
                })
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            patchClients: function (client, id) {

                var defer = $q.defer();
                $http.patch(GetURLFactory.getURL() + API_TYPE._MEMBERSHIP_.APPLICATIONS + '/' + id, client)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            postClients: function (client) {

                var defer = $q.defer();
                $http.post(GetURLFactory.getURL() + API_TYPE._MEMBERSHIP_.APPLICATIONS, client)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            deleteClients: function (client) {

                var defer = $q.defer();
                $http.delete(GetURLFactory.getURL() + API_TYPE._MEMBERSHIP_.APPLICATIONS + '/' + client._id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
        }
    }
])
