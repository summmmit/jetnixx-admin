// persisting email as _user in the cookies for the user to identify in the sessions.
app.service('UserPersistenceService', ['$http', 'LOCAL_STORAGE', 'SESSION_AUTHENTICATION', 'APP_INFO_',
function ($http, LOCAL_STORAGE, SESSION_AUTHENTICATION, APP_INFO_) {

    var isAuthenticated = false;
    var authToken,
        access_token_name = APP_INFO_.ENVIRONMENT.NAME + LOCAL_STORAGE._KEY_,
        time = APP_INFO_.ENVIRONMENT.NAME + LOCAL_STORAGE._TIME_,
        refresh_token_name = APP_INFO_.ENVIRONMENT.NAME + LOCAL_STORAGE._REFRESH_;

    return {
        loadUserCredentials: function () {
            var token = this.getAccessToken();
            if (token) {
                this.useCredentials(token);
            }
        },
        storeUserCredentials: function (token_data) {
            window.localStorage.setItem(access_token_name, token_data.access_token);
            window.localStorage.setItem(time, moment().unix() + SESSION_AUTHENTICATION._ACCESS_TOKEN_EXPIRATION_TIME);
            window.localStorage.setItem(refresh_token_name, token_data.refresh_token);
            this.useCredentials(token_data.access_token);
        },
        setTokenExpirationTime: function () {
            window.localStorage.setItem(time, moment().unix() + SESSION_AUTHENTICATION._ACCESS_TOKEN_EXPIRATION_TIME);
        },
        getTokenExpirationTime: function () {
            return window.localStorage.getItem(time);
        },
        getAccessToken: function () {
            return window.localStorage.getItem(access_token_name);
        },
        getRefreshToken: function () {
            return window.localStorage.getItem(refresh_token_name);
        },
        useCredentials: function (token) {
            isAuthenticated = true;
            // Set the token as header for your requests!
            $http.defaults.headers.common.Authorization = "Bearer " + token;
        },
        destroyUserCredentials: function () {
            authToken = undefined;
            isAuthenticated = false;
            $http.defaults.headers.common.Authorization = undefined;
            window.localStorage.removeItem(access_token_name);
            window.localStorage.removeItem(time);
            window.localStorage.removeItem(refresh_token_name);
        },
        isAuthenticated: function () {

            if (this.getTokenExpirationTime() < moment().unix()) {
                return false; // Token Expired
            }
            return true; // Token not expired
        }
    }
}])
