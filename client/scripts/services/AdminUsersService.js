// persisting email as _user in the cookies for the user to identify in the sessions.
app.service('AdminUsersService', ['$http', '$q', 'API_TYPE', 'GetURLFactory',
    function ($http, $q, API_TYPE, GetURLFactory) {

        return {
            getAllAdminUsers: function (limit, offset) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.USERS, {
                        params: {
                            limit: limit,
                            offset: offset
                        }
                    })
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            getUserByUserId: function (user_id) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.USERS + '/' + user_id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;

            },
            putChangeUserStatus: function (user_id, user_status) {
                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.USER_STATUS + '/' + user_id, {
                        status: user_status
                    })
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            getActiveSessionUsers: function (user_id) {
                var defer = $q.defer();

                $http.get(GetURLFactory.getURL() + API_TYPE._ADMIN_.USER_SESSIONS + '/' + user_id)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result.data);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            putAdminUserInfo: function (user_id, user_info) {
                console.log(user_id)
                console.log(user_info)
                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.USERS + '/' + user_id, user_info)
                    .then(
                        // success
                        function (response) {
                            console.log(response)
                            if (response.data.result.statusCode == 200) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            postChangePassword: function (user_id, user_info) {
                var defer = $q.defer();
                $http.post(GetURLFactory.getURL() + API_TYPE._ADMIN_.PASSWORD + '/' + user_id, user_info)
                    .then(
                        // success
                        function (response) {
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            putContactNumber: function (user_id, user_info) {
                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.CONTACT_NUMBER + '/' + user_id, user_info)
                    .then(
                        // success
                        function (response) {
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            },
            putEmailAddress: function (user_id, user_info) {
                var defer = $q.defer();
                $http.put(GetURLFactory.getURL() + API_TYPE._ADMIN_.EMAIL + '/' + user_id, user_info)
                    .then(
                        // success
                        function (response) {
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response.data.result);
                        }
                    );
                return defer.promise;
            }
        }
    }
])
