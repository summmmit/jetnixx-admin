'use strict';

app.service('InstallationService', ['$http', '$q', 'UserPersistenceService', 'IdentifyDeviceTypeFactory', 'HTTP_CODES', 'API_TYPE', 'envService', 'APP_INFO_',
    function ($http, $q, UserPersistenceService, IdentifyDeviceTypeFactory, HTTP_CODES, API_TYPE, envService, APP_INFO_) {

        var API_URL = envService.read('API_URL');

        return {
            getDatabaseConnection: function () {

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._ADMIN_.DATABASE)
                    .then(
                        // success
                        function (response) {
                            console.log(response)
                            if (response.status == HTTP_CODES.SUCCESS.OK) {
                            }
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            getHostConnection: function () {

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._ADMIN_.SERVER)
                    .then(
                        // success
                        function (response) {
                            if (response.status == HTTP_CODES.SUCCESS.OK) {
                            }
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            createOAuthClient: function (client) {

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._ADMIN_.CLIENTS, client)
                    .then(
                        // success
                        function (response) {
                            if (response.status == HTTP_CODES.SUCCESS.OK) {
                            }
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            checkInstallAppStatus: function () {
                var app = {
                    current_version: APP_INFO_.APP.SERVER_VERSION
                }
                var defer = $q.defer();
                $http.get(API_URL + API_TYPE._ADMIN_.APP_INSTALL + '/' + APP_INFO_.APP.SERVER_VERSION)
                    .then(
                        // success
                        function (response) {
                            if (response.data.result.statusCode == HTTP_CODES.SUCCESS.OK) {
                                defer.resolve(response.data.result);
                            }
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            },
            postInstallApp: function () {
                var app = {
                    current_version: APP_INFO_.APP.SERVER_VERSION
                }

                var defer = $q.defer();
                $http.post(API_URL + API_TYPE._ADMIN_.APP_INSTALL, app)
                    .then(
                        // success
                        function (response) {
                            if (response.status == HTTP_CODES.SUCCESS.OK) {
                            }
                            defer.resolve(response.data.result);
                        },
                        // failed
                        function (response) {
                            //user = false;
                            defer.reject(response);
                        }
                    );
                return defer.promise;
            }
        };
    }
])
