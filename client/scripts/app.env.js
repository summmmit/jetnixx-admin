'use strict';
angular.module('app')

.constant("APP_INFO_", {
    "APP": {
        "CLIENT_VERSION": "0.0.1",
        "SERVER_VERSION": "0.0.1"
    },
    "ENVIRONMENT": {
        "TYPE": "development",
        "NAME": "development_shivi",
        "DEBUG": true,
        "KEY": "developmentSessionSecret",
    },
    "HOST_INFO": {
        "HOST": "https://administration-shivi-summmmit.c9users.io",
        "PORT": 80,
        "API_URL": "https://administration-shivi-summmmit.c9users.io",
        "STATIC_URL": "https://administration-shivi-summmmit.c9users.io/static"
    },
    "CLIENT_INFO": {
        "CLIENT_ID": "58a97b195d8ffd0e2663e32c",
        "CLIENT_SECRET": "$2a$08$J5q9pDH4bBzDtZipKb7lQe9GZTUTFC6BNvAw1yW3YlQDFCtC1N3pa"
    }
})
