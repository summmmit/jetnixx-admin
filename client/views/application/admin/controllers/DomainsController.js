// modal controller
app.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'domain', 'index', function ($scope, $modalInstance, domain, index) {

    if (domain) {
        $scope.domain = domain;
    }

    $scope.ok = function (domain) {
        $modalInstance.close(domain);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);

app.controller('NewDomainModalController', ['$scope', '$modalInstance', function ($scope, $modalInstance) {

    $scope.ok = function (domain) {
        $modalInstance.close(domain);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);
app.controller('DomainsController', ['$scope', '$modal', 'DomainService', '$log', function ($scope, $modal, DomainService, $log) {
        $scope.oneAtATime = true;

        DomainService.getAllDomains()
            .then(function (response) {
                $scope.DomainsList = response.DomainsList;
            })

        $scope.items = ['Item 1', 'Item 2', 'Item 3'];

        $scope.addItem = function () {
            var newItemNo = $scope.items.length + 1;
            $scope.items.push('Item ' + newItemNo);
        };

        $scope.status = {
            isFirstOpen: true,
            isFirstDisabled: false
        };

        $scope.items = ['item1', 'item2', 'item3'];

        $scope.addNewDomain = function () {

            var modalInstance = $modal.open({
                templateUrl: 'apps/admin/domainModal.html',
                controller: 'NewDomainModalController'
            });

            modalInstance.result.then(function (domain) {

                //TODO use service to send the domain data
                var length = $scope.DomainsList.length + 1;
                domain._id = length;
                $scope.DomainsList.push(domain)

            });
        };

        $scope.editDomain = function (index, domain) {

            var modalInstance = $modal.open({
                templateUrl: 'apps/admin/domainModal.html',
                controller: 'ModalInstanceCtrl',
                size: '',
                resolve: {
                    domain: function () {
                        return domain;
                    },
                    index: function () {
                        return index;
                    }
                }
            });

            modalInstance.result.then(function (domain) {
                console.log(domain);
                $scope.DomainsList[index] = domain;
            });
        }

        $scope.removeDomain = function (index) {
            $scope.DomainsList.splice(index, 1);
        }
    }]
);
app.filter('DomainLinkFilter', function () {
    return function (domain, Ip, Port) {
        if (domain) {
            return domain;
        } else {
            return 'http://' + Ip + ':' + Port;
        }
    }
});
app.filter('DomainTypeFilter', function () {
    return function (domainType) {
        if (domainType == 1) {
            return 'Development';
        } else if (domainType == 2) {
            return 'Media';
        } else if (domainType == 3) {
            return 'Other'
        }
    }
});
app.service('DomainService', function ($http, $q) {

    return {
        postDomains: function (domain) {
            var defer = $q.defer();
            var promise = $http.post('apps/admin/apis/domain-list.json',
                {
                    params: {'domain': domain}
                })
                .then(function (response) {
                    // var data = response.data;
                    defer.resolve(response.data.DomainsList);
                }, function (response) {
                    // return response.data;
                    $q.reject(response.data.result);
                })
            return defer.promise;
        },
        getAllDomains: function () {
            var promise = $http.get('apps/admin/apis/domain-list.json')
                .then(function (response) {
                    return response.data;
                })
            return promise;
        },
        editDomain: function (domain) {
            var promise = $http.post('apps/admin/apis/domain-list.json')
                .then(function (response) {
                    return response.data;
                })
            return promise;
        },
        deleteDomain: function (domain) {
            var promise = $http.post('apps/admin/apis/domain-list.json')
                .then(function (response) {
                    return response.data;
                })
            return promise;
        },
        getOneDomain: function (domain) {
            var promise = $http.post('apps/admin/apis/domain-list.json')
                .then(function (response) {
                    return response.data;
                })
            return promise;
        }
    };

})








